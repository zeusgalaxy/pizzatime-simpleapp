package com.example.pizzatime.activity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.example.pizzatime.activity.GoogleMapActivity;
import com.example.pizzatime.service.GooglePlaces;
import com.example.pizzatime.utils.ContentCache;
import com.example.pizzatime.utils.GPSTracker;
import com.example.pizzatime.utils.ConnectionDetector;
import com.example.pizzatime.utils.AlertDialogManager;

import com.example.pizzatime.R;
import com.example.pizzatime.R.layout;
import com.example.pizzatime.R.menu;
import com.example.pizzatime.adapters.PlacesListAdapter;
import com.example.pizzatime.container.Content;
import com.example.pizzatime.handlers.GooglePlacesHandler;
import com.example.pizzatime.utils.ObjectSerializer;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;

public class PlacesListActivity extends ListActivity {

	// flag for Internet connection status
	Boolean isInternetPresent = false;
	
	// Connection detector class
	ConnectionDetector cd;
	
	// GPS Location
	GPSTracker gps;
	double lat;
	double lng;
	
	// Alert Dialog Manager
	AlertDialogManager alert = new AlertDialogManager();	
	
	// Scrolling threshold
	public static final int SCROLLING_THRESHOLD = 5;
	
	// SharedPreferences / cache lookup key
	public static final String BUNDLE_CACHE = "BUNDLE_CACHE";
	public static final String PLACESLIST_CACHE = "placesList.Cache";
	public static final String VIEWPOS_CACHE = "viewPos.Cache";
	public static final String VIEWTOP_CACHE = "viewTop.Cache";
	
	public static final String CURRENT_LOC_LAT = "current_loc_lat";
	public static final String CURRENT_LOC_LNG = "current_loc_lng";
	public static final String PLACE_LOC = "place_loc"; 
	
	SharedPreferences preferences;
	
    PlacesListAdapter adapter;
     
    ArrayList<Content> content;
    
    String moreContent;
    
    // listview position
    int index = 0;
    int top   = 0;
    
    protected ProgressDialog progressDialog;
    
    GooglePlacesHandler placesHandler = new GooglePlacesHandler();
    
    // handler
    final private Handler handler = new Handler();

    public Context currentContext = PlacesListActivity.this;
    
    /** Inner class for completing load work */
    private Runnable finishedLoadingListTask = new Runnable() 
    {
        public void run() 
        {
          finishedLoadingList();
        }
    };    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pizza_places);
		
		//
		// Check if Internet present
		//
		cd = new ConnectionDetector(getApplicationContext());
		isInternetPresent = cd.isConnectingToInternet();
		if (!isInternetPresent) {
			// Internet Connection is not present
			alert.showAlertDialog(this, "Internet Connection Error",
					"Please connect to working Internet connection", false);
			// stop executing code by return
			return;
		}
		
		//
		// creating GPS Class object
		//
		gps = new GPSTracker(this);

		// check if GPS location can get
		if (gps.canGetLocation()) {
			lat = gps.getLatitude();
			lng = gps.getLongitude();
			Log.d("Your Location", "latitude:" + gps.getLatitude() + ", longitude: " + gps.getLongitude());
		} else {
			// Can't get user's current location
			alert.showAlertDialog(this, "GPS Status",
					"Couldn't get location information. Please enable GPS",
					false);
			// stop executing code by return
			return;
		}
		
		// Shared preferences
		SharedPreferences prefs = getPreferences(MODE_PRIVATE);
		
		content = (ArrayList<Content>)ContentCache.getObject(BUNDLE_CACHE);
        if(content == null && savedInstanceState != null)
        {
            content = (ArrayList<Content>)savedInstanceState.getSerializable(BUNDLE_CACHE);
        }
        // Restore from previous shared preference save
        else if (content == null && prefs.contains(PLACESLIST_CACHE)) {
        	
        	content = new ArrayList<Content>();
			try {
				content = (ArrayList<Content>) ObjectSerializer.deserialize(
																prefs.getString(PLACESLIST_CACHE, 
																ObjectSerializer.serialize(new ArrayList<Content>())
																));
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// view position
			index = prefs.getInt(VIEWPOS_CACHE, 0);
			top = prefs.getInt(VIEWTOP_CACHE, 0);
        }
        
        if(content == null)
        {
            // no previous data, so Google Places feed must be read
            readFeed();
        }
        else
        {
            // reuse existing feed data
            adapter = new PlacesListAdapter(currentContext, content);
            setListAdapter(adapter);
            
            // restore previous view position
            if (index > 0)
            	getListView().setSelectionFromTop(index, top);
        }
        
        // Animate the List View
        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(25);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f,Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, -1.0f,Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(300);
        set.addAnimation(animation);

        LayoutAnimationController controller = new LayoutAnimationController(set, 0.5f);
        ListView listView = getListView();        
        listView.setLayoutAnimation(controller);        
        
        //
        // On end of scroll and we need more data.
        getListView().setOnScrollListener(new OnScrollListener () {

		    private int visibleThreshold = SCROLLING_THRESHOLD;
		    private int currentPage = 0;
		    private int previousTotal = 0;
		    private boolean loading = true;

		    @Override
		    public void onScroll(AbsListView view, int firstVisibleItem,
		            int visibleItemCount, int totalItemCount) {
		        if (loading) {
		            if (totalItemCount > previousTotal) {
		                loading = false;
		                previousTotal = totalItemCount;
		                currentPage++;
		            }
		        }
		        if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
		            loading = true;
	            	if (moreContent != null) {
	            		
	            		// save index and top position
	            		index = ((ListView) view).getFirstVisiblePosition();
	            		top = ((ListView) view).getTop();
	            		
            			// Continue read google places feed if there are more results
	            		readFeed();
	            		
	            	}
		        }
		        
		        // hide progress dialog
		        //if (progressDialog.isShowing()) {
		        //	progressDialog.dismiss();
		        //}
		    }

			@Override
		    public void onScrollStateChanged(AbsListView view, int scrollState) {
		    }
		});
	}

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        //Log.d("PlacesListActivity.onSaveInstanceState()", "saving data");
        ContentCache.setObject(BUNDLE_CACHE, content);
    }
    
	@Override
	protected void onStop() {
	    super.onStop();
	    
		preferences  = getPreferences(MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		
        try {
            editor.putString(PLACESLIST_CACHE, ObjectSerializer.serialize(content));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        editor.putInt(VIEWPOS_CACHE, getListView().getFirstVisiblePosition());
        editor.putInt(VIEWTOP_CACHE, getListView().getTop());
        editor.commit();
	}    
    
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pizza_places, menu);
		return true;
	}
	
    // Handles selection of an item in the Places list
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) 
    {
        Content content = (Content)getListView().getItemAtPosition(position);
        Intent intent = new Intent(currentContext, GoogleMapActivity.class);
        
		// Sending current geo location
		//intent.putExtra(CURRENT_LOC_LAT, "37.8006144");
		//intent.putExtra(CURRENT_LOC_LNG, "-122.2503685");
		intent.putExtra(CURRENT_LOC_LAT, Double.toString(lat));
		intent.putExtra(CURRENT_LOC_LNG, Double.toString(lng));
		
		// passing near places to map activity
		intent.putExtra(PLACE_LOC, content);
        
        startActivity(intent);        
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
    	// Clear shared preferences
    	if (item.getItemId() == R.id.clearlist) {
    		preferences  = getPreferences(MODE_PRIVATE);
    		SharedPreferences.Editor editor = preferences.edit();
    		
    		editor.clear();
    		editor.commit();
    		
    		// regenerated the data and re-position the view.
    		content = null;
    		readFeed();
    		
            // Restore view position
            getListView().setSelectionFromTop(0, 0);
        }	   
    	
        return super.onOptionsItemSelected(item);
    }    
            
    // Actions after list is loaded in View
    protected void finishedLoadingList() 
    {
        setListAdapter(adapter);
        getListView().setSelection(0);
        getListView().setSaveEnabled(true);
        getListView().setClickable(true);
        progressDialog.dismiss();
        
        // Restore view position
        getListView().setSelectionFromTop(index, top);
    }
	
    // reads feed in a separate thread.  Starts progress dialog
    protected void readFeed()
    {
    	if (content == null) { 
    		progressDialog = ProgressDialog.show(
    				currentContext,
    				null,
    				getResources().getString(R.string.loading_places)
    				);
    	}
    	else {
    		// Store the current view position
    		index = getListView().getFirstVisiblePosition();
    		top = getListView().getTop();    		
    	}
    	
        Thread loadFeedThread = new Thread() 
        {
        	public void run() {
                           
              // Read Google places feed
              content = placesHandler.parseFeed(getApplicationContext(), lat, lng);
              moreContent = placesHandler.getNextPageToken();
              
              fillData(content);
              handler.post(finishedLoadingListTask);
          }
        };
        loadFeedThread.start();
        
    }
    
    // Loads feed data into adapter on initial reading of feed
    private void fillData(ArrayList<Content> contentList) 
    {
        //Log.d("PlacesListActivity", "fillData() called");
        adapter = new PlacesListAdapter(currentContext, contentList);
    }
       
}
