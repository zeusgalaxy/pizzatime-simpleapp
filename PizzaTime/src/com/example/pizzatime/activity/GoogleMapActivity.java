package com.example.pizzatime.activity;

import com.example.pizzatime.R;
import com.example.pizzatime.R.layout;
import com.example.pizzatime.R.menu;
import com.example.pizzatime.container.Content;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class GoogleMapActivity extends Activity {

	public static final String CURRENT_LOC_LAT = "current_loc_lat";
	public static final String CURRENT_LOC_LNG = "current_loc_lng";
	public static final String PLACE_LOC = "place_loc"; 	
	
	private GoogleMap map;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_google_map);
		
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		
		// Getting intent data
		Intent intent = getIntent();
		
		// Users current geo location
		String user_latitude = intent.getStringExtra(CURRENT_LOC_LAT);
		String user_longitude = intent.getStringExtra(CURRENT_LOC_LNG);
		
		// result place
		Content place = (Content) intent.getSerializableExtra(PLACE_LOC);
		
		double latitude = Double.parseDouble(user_latitude); // latitude
		double longitude = Double.parseDouble(user_longitude); // longitude
		
		// Current location marker 
		Marker user = map.addMarker(new MarkerOptions()
							.position(new LatLng(latitude, longitude))
							.title("Me")
							.snippet("My Current Location")
							.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.mark_red)));
		
		// Pizza marker
		Marker pizza = map.addMarker(new MarkerOptions()
							.position(new LatLng(place.getLat(), place.getLng()))
							.title(place.getName())
							.snippet(place.getVicinity())
							.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.pizza)));		
		
		// Creating a LatLng object for the current location
		LatLng latLng = new LatLng(Double.parseDouble(user_latitude), Double.parseDouble(user_longitude));
		
		// Move the camera instantly to current location with a zoom of 15.
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
		
	    // Zoom in, animating the camera.
	    map.animateCamera(CameraUpdateFactory.zoomTo(10), 100, null);		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.google_map, menu);
		return true;
	}

}
