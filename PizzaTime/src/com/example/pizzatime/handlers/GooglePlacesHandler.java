package com.example.pizzatime.handlers;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import com.example.pizzatime.R;

import com.example.pizzatime.container.PlacesList;
import com.example.pizzatime.container.Place;
import com.example.pizzatime.container.Content;
import com.example.pizzatime.service.GooglePlaces;
import com.example.pizzatime.utils.AlertDialogManager;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

public class GooglePlacesHandler {
	
	// Separeate your place types by PIPE symbol "|"
    // If you want all types places make it as null
    // Check list of types supported by google
	// 
	final public static String GOOGLE_PLACES_TYEES = "food|restaurant";
	final public static String GOOGLE_PLACES_NAME = "pizza";
	final public static double GOOGLE_PLACES_DISTANCE = 10 * 1000;  // search distance 10km
	
	// Places List
	PlacesList nearPlaces;
	
	private String next_page_token;
	
    // List for storing Content objects from Google Places feed
    private ArrayList<Content> contentList = new ArrayList<Content>();
	
	// KEY Strings
	public static String KEY_REFERENCE = "reference";   // id of the place
	public static String KEY_NAME = "name"; 			// name of the place
	public static String KEY_VICINITY = "vicinity";     // Place area name
	public static String KEY_ICON = "icon";             // image icon for the place
        
    // Parses Google Places feed and returns List of Content objects 
    // @return ArrayList of Content objects
    public ArrayList<Content> parseFeed(Context ctx, double lat, double lng) 
    {
    	GooglePlaces googlePlaces = new GooglePlaces();
    	
      	try {		
      		// get nearest places
      		nearPlaces = googlePlaces.search(lat, lng,
      				                         GOOGLE_PLACES_DISTANCE, 
      				                         GOOGLE_PLACES_TYEES, 
      				                         GOOGLE_PLACES_NAME,
      				                         next_page_token);
/*      				                         
      		nearPlaces = googlePlaces.search(37.8006144, -122.2503685, 
      										GOOGLE_PLACES_DISTANCE, 
      										GOOGLE_PLACES_TYEES, 
      										GOOGLE_PLACES_NAME, 
      										next_page_token);
*/		
      	} catch (Exception e) {
      		e.printStackTrace();
      	}
      	
		// Get json response status
		String status = nearPlaces.status;
		
		// Get json response next_page_token
		next_page_token = nearPlaces.next_page_token;
		
		// Alert Dialog Manager
		AlertDialogManager alert = new AlertDialogManager();
		
		// Check for all possible status
		if (status.equals("OK")) {
			// Successfully got places details
			if (nearPlaces.results != null) {
				// loop through each place
				for (Place p : nearPlaces.results) {
			
					// Store place data into list content
					Content place = new Content();
					
					place.setName(p.name);
					place.setVicinity(p.vicinity);
					place.setReference(p.reference);
					place.setIcon(p.icon);
					place.setLat(p.geometry.location.lat);
					place.setLng(p.geometry.location.lng);
					
					// set up the icon
					setIcon(place);
					
					contentList.add(place);
				}
			}
		}					
		else if(status.equals("ZERO_RESULTS")){
			// Zero results found
			alert.showAlertDialog(ctx.getApplicationContext(), "Near Places",
					"Sorry no places found. Try to change the types of places",
					false);
		}
		else if(status.equals("UNKNOWN_ERROR"))
		{
			alert.showAlertDialog(ctx.getApplicationContext(), "Places Error",
					"Sorry unknown error occured.",
					false);
		}
		else if(status.equals("OVER_QUERY_LIMIT"))
		{
			alert.showAlertDialog(ctx.getApplicationContext(), "Places Error",
					"Sorry query limit to google places is reached",
					false);
		}
		else if(status.equals("REQUEST_DENIED"))
		{
			alert.showAlertDialog(ctx.getApplicationContext(), "Places Error",
					"Sorry error occured. Request is denied",
					false);
		}
		else if(status.equals("INVALID_REQUEST"))
		{
			alert.showAlertDialog(ctx.getApplicationContext(), "Places Error",
					"Sorry error occured. Invalid Request",
					false);
		}
		else
		{
			alert.showAlertDialog(ctx.getApplicationContext(), "Places Error",
					"Sorry error occured.",
					false);
		}
		     	
        return contentList;
    }

    // Sets icon based on URL
    private void setIcon(Content content)
    { 
    	content.setIconDrawable(R.drawable.pizza);
    }
    
    public synchronized String getNextPageToken()
    {
    	return next_page_token;
    }
    
}
