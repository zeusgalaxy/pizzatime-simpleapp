package com.example.pizzatime.container;

import java.io.Serializable;
import java.net.URL;

import android.graphics.drawable.Drawable;

/** Class for holding Content details.  Used for places, collections and modules. */
public class Content implements Serializable
{
    /** id for serialization */
    public static final long serialVersionUID = 1L;
    
    /** Constructor */
    public Content()
    {
        
    }

    // reference of place
    private String reference;
    
    // name of place
    private String name;
    
    // icon of content
    private String icon;
       
    // vicinity
    private String vicinity;
    
    // latitude
    private double lat;
    
    // longitude
    private double lng;
    
    // database id
    private int id;
    
    private Drawable iconImage;
    
    private int iconDrawable = -1;
       
    public int getIconDrawable()
    {
        return iconDrawable;
    }

    public void setIconDrawable(int iconDrawable)
    {
        this.iconDrawable = iconDrawable;
    }

    public Drawable getIconImage()
    {
        return iconImage;
    }

    public void setIconImage(Drawable iconImage)
    {
        this.iconImage = iconImage;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getReference()
    {
    	return reference;
    }
    
    public void setReference(String reference)
    {
    	this.reference = reference;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getVicinity()
    {
        return vicinity;
    }

    public void setVicinity(String vicinity)
    {
        this.vicinity = vicinity;
    }    

    public String getIcon()
    {
        return icon;
    }

    public void setIcon(String icon)
    {
        this.icon = icon;
    }
    
    public double getLat()
    {
    	return lat;
    }
    
    public void setLat(double latitude)
    {
    	this.lat = latitude;
    }
    
    public double getLng()
    {
    	return lng;
    }
    
    public void setLng(double longitude)
    {
    	this.lng = longitude;
    }
}
