package com.example.pizzatime.adapters;

import com.example.pizzatime.R;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Class to hold view to improve performance of list rendering
 * @author Ed Woodward
 *
 */
public class ViewHolder
{
    
    //  image view for listview
    protected ImageView imageView;
    
    // TextView for title in ListView
    protected TextView textView;
    
    // TextView for other information in ListView
    protected TextView otherView;

    ViewHolder(View base)
    {
        imageView = (ImageView) base.findViewById(R.id.placeIcon);
        textView = (TextView) base.findViewById(R.id.placeName);
        otherView = (TextView) base.findViewById(R.id.vicinity);        
    }

}