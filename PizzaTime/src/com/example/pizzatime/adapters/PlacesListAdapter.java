package com.example.pizzatime.adapters;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import com.example.pizzatime.R;

import com.example.pizzatime.adapters.ViewHolder;
import com.example.pizzatime.container.Content;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class PlacesListAdapter extends ArrayAdapter<Content> {

    // Current context
    private Context context;

    // List of Content objects to display
    private ArrayList<Content> contentList;

    // View holder for list performance
    ViewHolder holder;	
	
	// Constructor
	public PlacesListAdapter(Context context, ArrayList <Content> contentList) {
        super(context, android.R.layout.simple_list_item_1, contentList);
        this.context = context;
        this.contentList = contentList;
        
 	   for (int idx = 0; idx < contentList.size(); idx++ ) {
		   Content c = contentList.get(idx);
		   c.setId(idx);
 	   }
        
        // ** Disable retrieve icon task, since serialization the contentList will
        // crash, we simply should not serialize images.
        // load image icons ...
        //new RetreiveIconTask().execute();
	}

    // Creates layout
   @Override
   public View getView(int position, View convertView, ViewGroup parent) 
   {
       View v = convertView;
       
       if (v == null) 
       {
           LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           v = vi.inflate(R.layout.place_list, null);
           holder = new ViewHolder(v);
           v.setTag(holder);
       }
       else
       {
           //Log.d("PlacesListAdapter.getView()", "view is NOT null ");
           holder= (ViewHolder)v.getTag();
           if(holder == null)
           {
               holder = new ViewHolder(v);
               v.setTag(holder);
           }
           
       }
       
       Content c = contentList.get(position);
       if(c != null)
       {
           //Log.d("LensListAdapter.getView()", "content is not null ");
           ImageView iv = holder.imageView;
           TextView text = holder.textView;
           TextView other = holder.otherView;
       
           // icon
           if (iv != null) {
       	       if(c.getIconDrawable() != -1) {
                   holder.imageView.setImageResource(c.getIconDrawable());
               }
               else
               {
                   holder.imageView.setImageDrawable(c.getIconImage());
               }
           }
           
           // name
           if (text != null) {
               holder.textView.setText(c.getName());
           }
           
           // vicinity
           if (other != null) {
               holder.otherView.setText(c.getVicinity());
           }        
       }
       return v;
   }	
     
   //  allows access to list of Content objects.
   //  Used so data can be stored by Activity when orientation is changed.
   //  Prevents data reload.
   //  
   //  @return ArrayList of Content objects
   //
   public ArrayList<Content> getItems()
   {
       return contentList;
   }
   
   //new RetrieiveIconTask().execute();
   public void loadIcons()
   {
	   for (int idx = 0; idx < contentList.size(); idx++ ) {
		   Content c = contentList.get(idx);
		   c.setId(idx);
		   
    	   Drawable drawable = LoadImageFromWebOperations(c.getIcon());
    	   if (drawable != null ) {
    		   c.setIconImage(drawable);
    		   c.setIconDrawable(-1);
    	   }   
	   }
   }
   
   public Drawable LoadImageFromWebOperations(String url) {

	    try {
	        InputStream is = (InputStream) new URL(url).getContent();
	        Drawable d = Drawable.createFromStream(is, "icon");
	        return d;
	    } catch (Exception e) {
	        System.out.println("Exc=" + e);
	        return null;
	    }

	}

   class RetreiveIconTask extends AsyncTask<String, Void, String> {

	    private Exception exception;

	    protected String doInBackground(String... urls) {
	        try {
	        	// load icons
	        	loadIcons();
	        } catch (Exception e) {
	            this.exception = e;
	        }
	        return null;
	    }
	}
}
